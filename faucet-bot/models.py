from peewee import *
from peewee import IntegrityError
from datetime import datetime, timedelta
import configparser

config = configparser.ConfigParser()
config.read('tbch_bot.config')

database_file = config["database"]["database_file"]
time_gap = int(config["database"]["time_gap"])

db = SqliteDatabase('local.db')

class Transactions(Model):
    _id = AutoField(unique=True)
    tg_id = IntegerField()
    tg_first_name = CharField(max_length=30)
    user_address = CharField(max_length=60)
    coin_type = CharField(max_length=10)
    amount_sent = DecimalField()
    success = BooleanField()
    tx_id = CharField(max_length=60)
    date = TimestampField()
	

    class Meta:
        database = db # This model uses the "db" database.

Transactions.create_table()

def save_transaction(tg_user_id, tg_first_name, user_address, coin_type, amount, transaction_status, tx_id):
    """ Checks if a Telegram user is present in the database.
    Returns True if a user is created, False otherwise.
    """
    db.connect(reuse_if_open=True)
    try:
        Transactions.create(
            tg_id=tg_user_id,
            tg_first_name=tg_first_name,
            coin_type = coin_type,
            amount_sent = amount,
            date = datetime.now(),
            success = transaction_status,
            user_address = user_address,
            tx_id = tx_id
        )
        db.close()
        return True
    except IntegrityError:
        db.close()
        return False

def recent_user(tg_id, coin_type):
    """check if the user have already requested the coin before 
    
    """
    # intervals allowed between requests
    db.connect(reuse_if_open=True)
    try:
        cutoff = datetime.now() - timedelta(hours=time_gap)
        query = Transactions.select().where((Transactions.date >= cutoff) & (Transactions.tg_id == tg_id) & (Transactions.coin_type == coin_type))
        if query.exists():
            return True
        else:
            return False
    except IntegrityError:
        db.close()
        return False

