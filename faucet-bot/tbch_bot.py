#!/usr/bin/env python
# pylint: disable=W0613, C0116
# type: ignore[union-attr]

# This program is published under the AGPL v3 license
# © 2021 gitlab.com/uak

# To do
# 1. Check that there is token balance before starting bot
# 2. Check that bot slp and bch wallets are loaded

# replaced notice from conversation handler bot

"""
BCH testnet coins faucet bot, you can setup this bot to hand people
test net coins.

Usage:
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import os
import sys
import logging
import configparser

from sys import exit
from decimal import Decimal
from datetime import datetime, timedelta


# Telegram bot imports
from telegram import (
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    Update,
    ParseMode,
)
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

# Importing from Electron Cash SLP command line lib
from ec_slp_lib import (
    check_daemon,
    check_wallet_loaded,
    get_bch_balance,
    bch_wallet_file,
    prepare_bch_transaction,
    prepare_slp_transaction,
    broadcast_tx,
)


from models import save_transaction, recent_user



# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,  # DEBUG #INFO
)

logger = logging.getLogger(__name__)


# Open configuration file
config = configparser.ConfigParser()
config.read('tbch_bot.config')


#electron_cash_path = config["wallet"]["electron_cash_path"]
bch_wallet_file = config["wallet"]["bch_wallet_file"]
tokenIdHex = config["settings"]["tokenIdHex"]
token = config["settings"]["token"]
donate = config["settings"]["donate"]
tbch_amount = config["settings"]["tbch_amount"]
token_amount = config["settings"]["token_amount"]
min_allowed_balance =  config["settings"]["min_allowed_balance"]
explorer = config["settings"]["explorer"]
time_gap = int(config["database"]["time_gap"])

# Check Electron Cash daemon running
daemon_running = check_daemon()
if not daemon_running:
    exit("daemon is not running") 

#check if the wallet is loaded, exit if not
wallet_loaded_bch = check_wallet_loaded(bch_wallet_file)

if not wallet_loaded_bch:
    exit("BCH wallet is not loaded") # Exit python if wallet not loaded
logger.info("Wallet loaded: %s", bch_wallet_file)


#check wallet token balance before start, if balance lower than minimum
# limit do not start.
bch_balance = get_bch_balance(bch_wallet_file)
balance = Decimal(bch_balance["confirmed"])

if Decimal(balance) >= Decimal(min_allowed_balance):
    logger.info("Balance is: %s", balance)
    pass
else:
    logger.info("No token balance: %s", balance)
    # Exit python if wallet not loaded
    exit()


# Those are by Telegram bot
CONFIRM, INFORMATION = range(2)


# Bot first message asking for token amount to buy
def start(update: Update, context: CallbackContext) -> int:
    """Start the bot
    Offer user to receive test bch coins or test slp coins
    """
    # Get bch balance
    balance = Decimal(bch_balance["confirmed"])
    # Check if there is enough balance in the wallet
    if Decimal(balance) > Decimal(min_allowed_balance):
        text = (
            "Hi, I can give you BCH test coins (tBCH) "
            "Please send your bchtest address to get tBCH.\n"
            "You can get a test address by using "
            '<a href="https://electroncash.org/">Electron Cash</a> '
            "wallet with <code>--testnet</code> flag, "
            'for support check our <a href="https://t.me/slpsell_bot">'
            "SLP bot group</a> \n"
            'If you want sTST SLP tokens instead send a slptest address\n'
            "⚠️ This bot is still under development use it at your own risk. \n"
            "Send /cancel to stop."
        )
        update.message.reply_text(text, parse_mode="html")
    else:
        text = (
            "Sorry, not enough tBCH in the faucet, please contact us"
            'on our <a href="https://t.me/slpsell_bot">SLP bot group</a>\n'
            "You can also donate to the address in the bot /info"
        )       
        update.message.reply_text(text, parse_mode="html")

    return CONFIRM


def prepare_broadcast_bch():
    # Prepare transaction using EC SLP lib
    # amount from tbch_amount value
    amount = tbch_amount
    tx_hex = prepare_bch_transaction(
        bch_wallet_file,
        recipient_address,
        amount,
    )
    
    # Brodacast transaction using EC SLP lib
    global tx_id
    tx_id = broadcast_tx(bch_wallet_file, tx_hex)
    return tx_id

def prepare_broadcast_slp():
    # Prepare transaction using EC SLP lib
    amount = token_amount
    tx_hex = prepare_slp_transaction(
        bch_wallet_file,
        tokenIdHex,
        recipient_address,
        amount,
    )
    
    # Brodacast transaction using EC SLP lib
    global tx_id
    tx_id = broadcast_tx(bch_wallet_file, tx_hex)
    return tx_id


def confirm(update: Update, context: CallbackContext) -> int:
    """
    Send the coin to user when they send an address
    """
    address = update.message.text
    global recipient_address

    # do basic validation of user address, send if valid
    if address[0:7] == "bchtest" and len(address) == 50 :
        coin_type = "bch"
        # check if user have asked for coins recently
        if not recent_user(update.message.from_user.id, coin_type):
            transaction_status = 1
            recipient_address = update.message.text
            # Broadcast transaction using Electron Cash wallet
            tx_id = prepare_broadcast_bch()
            amount = tbch_amount
            pass
        else:
            text = (
                    "You have already requested tBCH in the last"
                    " {} hours, You can ask later ".format(time_gap)
                    )
            update.message.reply_text(text,
                                parse_mode="html") 
            return CONFIRM

    # do basic validation of user address, send if valid
    elif address[0:7] == "slptest" and len(address) == 50:
        coin_type = "slp"
        transaction_status = 1
        recipient_address = update.message.text
        # Broadcast transaction using Electron Cash wallet
        tx_id = prepare_broadcast_slp()
        amount = token_amount
        pass
        
    else:
        update.message.reply_text("address is not vaild", parse_mode="html")  
        return CONFIRM
        

    # log user address
    logger.info("user address is: %s", update.message.text)
    
    text = (
        "You should have received {} {} to your address.\n"
        'Transaction ID is: <a href="{}{}">{}</a> \n'
        "send /start when you want more. Thank you.".format(amount, coin_type, explorer, tx_id, tx_id)
        )
    update.message.reply_text(text, parse_mode="html")        
    tg_user_id = update.message.from_user.id
    tg_user_name = update.message.from_user.first_name

    # save data to the database
    save_transaction(
                tg_user_id,
                tg_user_name,
                recipient_address,
                coin_type, amount,
                transaction_status,
                tx_id
                )
    logger.info("Transaction completed, ID: %s", tx_id)
    return ConversationHandler.END
 
def test1(update: Update, context: CallbackContext) -> int:
    """"print stuff when command sent"""
    update.message.reply_text("testy")
    return information

def information(update: Update, context: CallbackContext) -> int:
    """
    Print useful information about the bot
    """
    update.message.reply_text(
        "Thank you for your business! If you want"
         " to incourage developement of this bot and"
         " tip the dev plase donate to this campagin: {}".format(donate)
    )
    logger.info("User asked for info")

    return ConversationHandler.END


# Canceling conversation
def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        "Bye! I hope we can serve you again some day.",
        reply_markup=ReplyKeyboardRemove(),
    )

    return ConversationHandler.END


def main() -> None:
    # Create the Updater and pass it your bot's token.
    updater = Updater(token)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            CONFIRM: [
                MessageHandler(Filters.text & ~Filters.command, confirm)
            ],
            INFORMATION: [
                MessageHandler(Filters.text & ~Filters.command, information)
            ],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    msg_handler = CommandHandler(["info","information"], information)
    dispatcher.add_handler(conv_handler)
    dispatcher.add_handler(msg_handler)
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == "__main__":
    main()

