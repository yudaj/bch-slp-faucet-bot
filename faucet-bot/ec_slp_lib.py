import json
import subprocess
import sys

# Check if daemon is connected
def check_daemon(electron_cash_path):
    """Check daemon running
    Checks if Electron Cash daemon is running
    """
    daemon_status = subprocess.run(
        (electron_cash_path, "daemon", "status"),
        capture_output=True,
        text=True,
    )
    try:
        json_output = json.loads(daemon_status.stdout)
        connected = json_output["connected"]
        return True
    except ValueError:  # includes simplejson.decoder.JSONDecodeError
        print("daemon is not connected", file=sys.stderr)
        return False


# Doesn't support multiple loaded wallet
def check_wallet_loaded(electron_cash_path, wallet_path):
    """Check wallet loaded
    Checks if the wallet is loaded in Electron Cash daemon
    """
    daemon_status = subprocess.run(
        (electron_cash_path, "daemon", "status"),
        capture_output=True,
        text=True,
    )
    try:
        json_output = json.loads(daemon_status.stdout)
        if (wallet_path, True) in json_output["wallets"].items():
            return True
        else:
            return  False
    except:
        print("wallet not loaded", file=sys.stderr)
        return False


def get_unused_bch_address(electron_cash_path, wallet_path):
    """Get unused BCH address"""
    unused_bch_address = subprocess.run(
        (electron_cash_path, "-w", wallet_path, "getunusedaddress"),
        capture_output=True,
        text=True,
    )
    return unused_bch_address


def get_unused_slp_address(electron_cash_path, wallet_path):
    """Get unused SLP address"""
    receive_address = subprocess.run(
        (electron_cash_path, "-w", wallet_path, "getunusedaddress_slp"),
        capture_output=True,
        text=True,
    )
    return unused_slp_address


def get_address_balance_bch(electron_cash_path, wallet_path, address):
    """Get the balance of a BCH address"""
    bch_address_balance_raw = subprocess.run(
        (electron_cash_path, "-w", wallet_path, "getaddressbalance", address),
        capture_output=True,
        text=True,
    )
    try:
        bch_address_balance = json.loads(
            (bch_address_balance_raw.stdout).strip()
        )
        return bch_address_balance
    except:
        print("Decoding JSON has failed", file=sys.stderr)


def get_token_balance(electron_cash_path, wallet_path, tokenIdHex):
    """Get the balance of a SLP token in wallet"""
    token_balance_raw = subprocess.run(
        (electron_cash_path, "-w", wallet_path, "getbalance_slp", tokenIdHex),
        capture_output=True,
        text=True,
    )
    try:
        token_balance_json = json.loads((token_balance_raw.stdout).strip())
        token_balance = token_balance_json["valid"]
        return token_balance
    except:
        print("Decoding JSON has failed", file=sys.stderr)


def prepare_transaction(
    electron_cash_path, wallet_path, tokenIdHex, address, token_amount
):
    """Prepare transaction
    Creates the raw transaction data
    """
    tx_data = subprocess.run(
        (electron_cash_path, "-w", wallet_path, "payto", address, bch_amount),
        capture_output=True,
        text=True,
    )
    try:
        tx_data_json = json.loads(tx_data.stdout)
        return tx_data_json["hex"]
    except:
        print("Decoding JSON has failed", file=sys.stderr)


def prepare_slp_transaction(
    electron_cash_path, wallet_path, tokenIdHex, address, token_amount
):
    """Prepare SLP transaction
    Creates the raw SLP transaction data
    """
    tx_data = subprocess.run(
        (
            electron_cash_path,
            "-w",
            wallet_path,
            "payto_slp",
            tokenIdHex,
            address,
            token_amount,
        ),
        capture_output=True,
        text=True,
    )
    try:
        tx_data_json = json.loads(tx_data.stdout)
    except:
        print(tx_data.stderr, file=sys.stderr)
    return tx_data_json["hex"]


def broadcast_tx(electron_cash_path, wallet_path, tx_hex):
    """Broadcast transaction
    Send the transaction to the network
    """
    broadcast = subprocess.run(
        (electron_cash_path, "-w", wallet_path, "broadcast", tx_hex),
        capture_output=True,
        text=True,
    )
    try:
        tx_id_json = json.loads(broadcast.stdout)
        return tx_id_json[1]
    except:
        print("Decoding JSON has failed", file=sys.stderr)


def freeze_address(electron_cash_path, wallet_path, address):
    """
    Freeze an address so it can not be used later
    """
    freeze_raw = subprocess.run(
        (electron_cash_path, "-w", wallet_path, "freeze", address),
        capture_output=True,
        text=True,
    )
    try:
        freeze = json.loads(
            (freeze_raw.stdout).strip()
        )
        return freeze
    except:
        print("Decoding JSON has failed", file=sys.stderr)
